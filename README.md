# TD - Mise en place d'un docker

## Prérequis

Afin de suivre les différentes étapes de ce README, il vous faudra installer docker, avec par exemple, [Docker Desktop](https://www.docker.com/products/docker-desktop/).

Attention : Si vous êtes sur Windows, il faudra activer Hyper-V ou installer et activer [WSL2](https://learn.microsoft.com/en-us/windows/wsl/install).

---
## Récuperer les fichiers

Pour récuperer les fichiers de ce repository, il faudra faire un ```git clone``` ou télécharger le dossier .zip dans un dossier docker, situé dans votre dossier ```C:/Users/UtilisateurVoulu```.

---
## Mise en place des conteneurs

Docker utilise des images pour ses conteneurs, ces images servent d'instructions pour créer les différents conteneurs.

Les images utilisées ici sont les suivantes :

- dpage/pgadmin4
- postgres
- php avec un layer apache (php:8.2-apache)
- linuxserver/minetest

Vous pouvez utiliser la commande ``docker pull nomImage`` dans un terminal pour les installer manuellement.

Sinon, pour mettre en place les conteneurs en utilisant composer :
- ouvrez un terminal dans votre dossier docker : ``C:/Users/UtilisateurVoulu/docker``.
- tapez ensuite ``docker compose up`` pour créer et lancer les différents conteneurs.

---
## Variables d'environnement

Dans le fichier ``docker-compose.yml``, vous trouverez des variables d'environnement déjà configurées pour vous et que vous pouvez changer :
- ``postgres`` :
    - nom de la base de données : stella
    - nom de l'utilisateur : tp1
    - mot de passe de l'utilisateur : tp12023
    - dossier contenant les données : /data/postgres
- ``pgadmin`` :
  - mail admin par défaut : woravi2192@galcake.com (tempmail)
  - mot de passe admin par défaut : tp12023

---
## Dossiers

Plusieurs dossiers se créent également lors de la création des conteneurs. Ces dossiers sont des sauvegardes de vos données, dans le cas où vous supprimez les conteneurs (à l'initialisation des conteneurs, ces données sont inexistantes et c'est à vous de les "créer").

Il y en a actuellement 3 :
- Le dossier ``pgadmin``, contenant les informations sur la configuration et les données pgadmin (par exemple, le master password qu'on verra plus tard).
- Le dossier ``postgres``, contenant toutes les informations de la base de données (tables, données, utilisateurs...).
- Le dossier ``web``, contenant le dossier html, qui contiendra votre architecture du site web.


Attention : Si vous supprimez ces dossiers, vous perdrez les données. Seuls les conteneurs peuvent être supprimés sans risque.

---
## Utilisation de PGAdmin et Postgres

Pour lancer PGAdmin : http://localhost:8182/

Une fois sur la page, cette fenêtre apparait :

![img.png](images/pgadmin-masterpwd.png)

Définissez un mot de passe avec lequel vous accéderez à pgadmin (celui-ci sert à ajouter une couche de sécurité et donne accès à des données sensibles comme les mots de passe du serveur BDD).

Pour se connecter à la base de données, dirigez vous vers l'onglet Servers => Register => Server...

![img.png](images/pgadmin-server.png)

- L'option "Name" de l'onglet Général est le nom qu'aura le serveur BDD sur votre interface, vous pouvez donc le choisir vous-même.
- L'onglet Connection, quant-à lui, doit être rempli de cette façon (selon la configuration générale des variables d'environnement postgres) : 

![img.png](images/pgadmin-serversetup.png)

Après avoir cliqué sur "Save", vous aurez accès à la base de données postgres et pourrez la modifier !

---
## Utilisation du serveur web

Pour utiliser le serveur web, il suffit de mettre vos fichiers dans le dossier web et le dossier html.

Les fichiers du dossier web seront principalement des dossiers de configuration.

Les fichiers du dossier html seront accessibles sur un lien comme celui-ci : http://localhost:8180/fichier.extension.

## Utilisation de Minetest

Tout d'abord, vous aurez besoin de télécharger l'application [Minetest](https://www.minetest.net/downloads/), cette application est un jeu ressemblant grandement au jeu Minecraft.

Une fois le jeu lancé, rentrez les informations suivantes :

![img.png](images/minetest-setup.png)

Le port est modifiable dans le fichier ``docker-compose.yml``, quant-au nom et au mot de passe, c'est à vous de les définir dans le jeu.

Vous pourrez alors jouer sur votre serveur.
